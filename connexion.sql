-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mar. 28 avr. 2020 à 16:57
-- Version du serveur :  5.7.24
-- Version de PHP : 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `db_medecin`
--

-- --------------------------------------------------------

--
-- Structure de la table `connexion`
--

CREATE TABLE `connexion` (
  `ID` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `connexion`
--

INSERT INTO `connexion` (`ID`, `nom`, `prenom`, `email`, `password`) VALUES
(1, 'root', 'root', 'root@gmail.com', '$2y$10$5O.9S3evkbKWlXf6/rmoAu/YTkDZuLnp3UfadUtKh2Laor2It7GFa'),
(2, 'rootb', 'roota', 'root@hotmail.fr', '$2y$10$87W1yxAGxHvvYYR9mRUPm.Et9eW7.TYYa1R63yDDd3HKt2Qr8PFdu'),
(3, 'test', 'abc', 'abc@gmail.com', '$2y$10$ypOyeFl3vdxK0W1G6z57aOXihngYwHM2dHaUUunBKLW9rl18AYw2K'),
(9, 'roota', 'rootb', 'roota@gmail.com', '$2y$10$zX3Vc9brT713Ca93YoPu/OHwEjA/q0.u/Av7aDXFq2PFfnzmhvJz6'),
(10, 'aaazz', 'aaazzzzz', 'testaaa@gmail.com', '$2y$10$3Foxn2X/Gy2SlxRK/ALN.ebKhciHwUbP84oSngwp1ffzcKwrPirZ6'),
(11, 'aaaaaatt', 'tttatatata', 'testaaaaa@gmail.com', '$2y$10$VFhqU5njgKXTG5EUZz.kKOYFub9xvksWmQzVmv.5HlZ2wECNl.cwW'),
(12, 'aaaaaaaaaaaa', 'aaaaaaaaaaa', 'aaaaaaaaa@gmail.com', '$2y$10$gyjK3xwmbBI0eoGVowz5UONF0qx7GbNsxgrsT3yORYSEpuDa/RF1.');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `connexion`
--
ALTER TABLE `connexion`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `connexion`
--
ALTER TABLE `connexion`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
