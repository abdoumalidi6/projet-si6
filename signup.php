<?php
require('config.php');

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Créer un compte</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-4 form-div login">
            <form action="enregistrement.php" method="post">
            <h3 class="text-center">Enregistrez vous</h3>

         

            <div class="form-group">
            <label for="nom">Nom</label>
            <input type="text" name="nom" class="form-control form-control-lg">
            </div>

            <div class="form-group">
            <label for="prenom">Prénom</label>
            <input type="text" name="prenom" class="form-control form-control-lg">
            </div>

            <div class="form-group">
            <label for="email">E-mail</label>
            <input type="email" name="email" class="form-control form-control-lg">
            </div>

            <div class="form-group">
            <label for="password">Mot de passe</label>
            <input type="password" name="password" class="form-control form-control-lg">
            </div>

            <div class="form-group">
            <label for="passwordConf">Confirmez votre mot de passe</label>
            <input type="password" name="passwordConf" class="form-control form-control-lg">
            </div>

            <div class="form-group">
            <button type="submit" name="signup-btn" class="btn btn-primary btn-block btn-lg">S'enregistrer</button>
            </div>
            <p class="text-center">Vous avez déjà un compte ? <a href="login.php">Connectez vous!</a> </p>
            </div>
            </form>
            </div>
        </div>
    </div>
</body>
</html>