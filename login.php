<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Connexion</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 offset-md-4 form-div login">
            <form action="connexion.php" method="post">
            <h3 class="text-center">Connexion</h3>

            <div class="form-group">
            <label for="email">E-mail</label>
            <input type="text" name="email" class="form-control form-control-lg">
            </div>

            <div class="form-group">
            <label for="password">Mot de passe</label>
            <input type="password" name="password" class="form-control form-control-lg">
            </div>


            <div class="form-group">
            <button type="submit" name="login-button" class="btn btn-primary btn-block btn-lg">Connexion</button>
            </div>
            <p class="text-center">Vous n'avez pas encore de compte? <a href="signup.php">Inscrivez vous!</a> </p>

            </form>
            </div>
        </div>
    </div>
</body>
</html>