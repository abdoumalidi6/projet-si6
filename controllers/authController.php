<?php

session_start();


require 'config/db.php';



$errors = array();
$nomutilisateur = "";
$email = "";

// Si l'utilisateur clique sur the boutton d'authentification 
if (isset($_POST['signup-btn'])){
    $nomutilisateur = $POST['nomutilisateur'];
    $email = $POST['email'];
    $password = $POST['password'];
    $passwordConf = $POST['passwordConf'];

    // Validation de chaque variable 
    if(empty($nomutilisateur)){
        $errors['nomutilisateur'] = "Veuillez saisir un nom d'utilisateur.";
    }
    if (!filter_var($email,FILTER_VALIDATE_EMAIL)){
        $errors['email'] = "Veuillez saisir une adresse e-mail valide.";
    }
    if(empty($email)){
        $errors['email'] = "Veuillez saisir une adresse e-mail.";
    }
    if(empty($password)){
        $errors['password'] = "Veuillez saisir un mot de passe.";
    }
    if(empty($password !== $passwordConf)){
        $errors['password'] = "Les mots de passes sont différents.";
    }

    $emailQuery = "SELECT * FROM utilisateurs WHERE email=? LIMIT 1";
    $stmt = $conn->prepare($emailQuery);
    $stmt->bind_param('s',$email);
    $stmt->execute();
    $result = $stmt->get_result();
    $userCounter = $result->num_rows;
    $stmt->close();



    if($userCounter > 0){
        $errors['email'] = "Cette e-mail est déjà utilisée.";
    }


    if(count($errors) == 0) {
        $password = password_hash($password, PASSWORD_DEFAULT);
        $token = bin2hex(random_bytes(50));
        $verifier = false;


        $sql = "INSERT INTO utilisateurs (nomutilisateur, email, verifier, token, password) VALUES(?, ?, ?, ?, ?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param('ssbss', $nomutilisateur, $email, $verifier, $token, $password);

        if ($stmt->execute()){
            //Connexion de l'utilisateur 
            $user_id = $conn->insert_id;
            $_SESSION['id'] = $user_id;
            $_SESSION['nomutilisateur'] = $nomutilisateur;
            $_SESSION['email'] = $email;
            $_SESSION['verifier'] = $verifier;
            header('location: index.php');
            exit();

            // Message de "confirmation"
            $_SESSION['message'] = "Vous êtes bien connecté!";
            $_SESSION['alert-class'] = "alert-success";

        } else {
            $errors['db_error'] = "Erreur dans la base de donnée : Pas d'enregistrement"
        }


    }



}