<?php
require('config.php');

?>
<!DOCTYPE html>

<html lang="en">
  <head>
    <title>NomDuSite</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">
    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
  	<div class="py-1 bg-black top">
    	<div class="container">
    		<div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
	    		<div class="col-lg-12 d-block">
		    		<div class="row d-flex">
		    			<div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
						    <span class="text">01 00 00 00 00</span>
					    </div>
					    <div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
						    <span class="text">NomDuSite@gmail.com</span>
					    </div>
						<?php if (isset($_COOKIE['password']))
						{
							 echo"<div class='col-md-5 pr-4 d-flex topper align-items-center text-lg-right justify-content-end'>
						    <p class='mb-0 register-link'> <a href='deco.php'>Deconnexion</a> </p>
						</div>";
						}
						else{
							echo"<div class='col-md-5 pr-4 d-flex topper align-items-center text-lg-right justify-content-end'>
						    <p class='mb-0 register-link'><a href='signup.php'>Inscription</a> <a href='login.php'>Connexion</a> </p>
						</div>";
						}
						?>
				    </div>
			    </div>
		    </div>
		  </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light site-navbar-target" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="index.html">NomDuSite</a>
	      <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav nav ml-auto">
	          <li class="nav-item"><a href="#home-section" class="nav-link"><span>Accueil</span></a></li>
	          <li class="nav-item"><a href="#about-section" class="nav-link"><span>À propos</span></a></li>
	          <li class="nav-item"><a href="#department-section" class="nav-link"><span>Nos domaines</span></a></li>
	          <li class="nav-item"><a href="#doctor-section" class="nav-link"><span>Nos medecins</span></a></li>
	          <li class="nav-item"><a href="#blog-section" class="nav-link"><span>Articles</span></a></li>
	          <li class="nav-item"><a href="#contact-section" class="nav-link"><span>Contacte</span></a></li>
			  <?php if (isset($_COOKIE['password']))
                        {
                             echo'<li class="nav-item cta mr-md-2"><a href="Formulaire_de_réservation_médicale_4/Formulaire_de_réservation_médicale.php" class="nav-link">Prise de rendez-vous</a></li>';
                        }
                        else{
                            echo'<li class="nav-item cta mr-md-2"><a href="login.php" class="nav-link">Connectez vous</a></li>';
                        }
                ?>
	        </ul>
	      </div>
	    </div>
	  </nav>
	  
	  <section class="hero-wrap js-fullheight" style="background-image: url('images/bg_3.jpg');" data-section="home" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-md-6 pt-5 ftco-animate">
          	<div class="mt-5">
          		<span class="subheading">Bienvenue.</span>
	            <h1 class="mb-4">Simple, gratuit et rapide</h1>
	            <p class="mb-4">Vous souhaitez prendre rendez-vous avec des profession de santé? Notre site est fait pour vous, peu importe le secteur dans lequel vous souhaitez être traité, nous vous trouverons des médecins à votre disposition dans l’Île-de-France.</p>
				<?php if (isset($_COOKIE['password']))
                        {
                             echo'<p><a href="Formulaire_de_réservation_médicale_4/Formulaire_de_réservation_médicale.php" class="btn btn-primary py-3 px-4">Prendre rendez-vous en un clic.</a></p>';
                        }
                        else{
                            echo'<p><a href="Formulaire_de_réservation_médicale_4/Formulaire_de_réservation_médicale.php" class="btn btn-primary py-3 px-4">Connectez vous pour prendre un rendez-vous.</a></p>';
						}
					?>
	            
            </div>
          </div>
        </div>
      </div>
    </section>

		<section class="ftco-counter img ftco-section ftco-no-pt ftco-no-pb" id="about-section">
    	<div class="container">
    		<div class="row d-flex">
    			<div class="col-md-6 col-lg-5 d-flex">
    				<div class="img d-flex align-self-stretch align-items-center" style="background-image:url(images/about.jpg);">
    				</div>
    			</div>
    			<div class="col-md-6 col-lg-7 pl-lg-5 py-md-5">
    				<div class="py-md-5">
	    				<div class="row justify-content-start pb-3">
			          <div class="col-md-12 heading-section ftco-animate p-4 p-lg-5">
			            <h2 class="mb-4">Des medecins à votre écoute.</h2>
			            <p>Nos medecins sont à l'écoute du moindre problème que vous pouvez rencontrer.</p>
			            <p><a href="#contact-section" class="btn btn-secondary py-3 px-4">Nous contacter</a></p>
			          </div>
			        </div>
		        </div>
	        </div>
        </div>
    	</div>
    </section>


		

    <section class="ftco-intro img" style="background-image: url(images/bg_2.jpg);">
			<div class="overlay"></div>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-9 text-center">
						<h2>Votre santé est notre priorité.</h2>
						<p>Pensez à vous laver les mains régulièrement pour éviter la propagation des bactéries sur chaque objet que vous touchez. Pour une bonne hygiène de vie, préférez les escaliers aux ascenseurs, et le vélo à la voiture.</p>
					</div>
				</div>
			</div>
		</section>

		<section class="ftco-section ftco-no-pt ftco-no-pb" id="department-section">
    	<div class="container-fluid px-0">
    		<div class="row no-gutters">
    			<div class="col-md-4 d-flex">
    				<div class="img img-dept align-self-stretch" style="background-image: url(images/dept-1.jpg);"></div>
    			</div>

    			<div class="col-md-8">
    				<div class="row no-gutters">
    					<div class="col-md-4">
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a href="#">Neurologie</a></h3>
    								<p>Nous travaillons avec des médecins dans le domaine de la neurologie.</p>
    							</div>
    						</div>
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a href="#">Chirurgie</a></h3>
    								<p>Nous travaillons avec des chirurgiens.</p>
    							</div>
    						</div>
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a href="#">Allergologie</a></h3>
    								<p>Nous travaillons avec des allergologues d'exception, ils sauront vous porter secours.</p>
    							</div>
    						</div>
    					</div>

    					<div class="col-md-4">
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a href="#">Opthalmologie</a></h3>
    								<p>Nous travaillons avec des médecins expérimentés en opthalmologie.</p>
    							</div>
    						</div>
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a href="#">Cardiologie</a></h3>
    								<p>Nous travaillons avec des médecins experts en cardiologie.</p>
    							</div>
    						</div>
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a href="#">Dermatologue</a></h3>
    								<p>Nous travaillons avec des dermatologue de confiance.</p>
    							</div>
    						</div>
    					</div>

    					<div class="col-md-4">
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a href="#">Psychologie</a></h3>
    								<p>Nous travaillons avec des psychologues prêt à vous écouter.</p>
    							</div>
    						</div>
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a href="#">Dentisterie</a></h3>
    								<p>Nous travaillons avec des médecins dans le domaine de la dentition.</p>
    							</div>
    						</div>
    						<div class="department-wrap p-4 ftco-animate">
    							<div class="text p-2 text-center">
    								<div class="icon">
    									<span class="flaticon-stethoscope"></span>
    								</div>
    								<h3><a href="#">Pédiatrie</a></h3>
    								<p>Nous travaillons depuis peu avec des pédiatres.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </section>
		
		<section class="ftco-section" id="doctor-section">
			<div class="container-fluid px-5">
				<div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-8 text-center heading-section ftco-animate">
            <h2 class="mb-4">Nos docteurs les plus connus.</h2>
            <p>Des medecins à votre écoute</p>
          </div>
        </div>	
				<div class="row">
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/doc-1.jpg);"></div>
							</div>
							<div class="text pt-3 text-center">
								<h3 class="mb-2">Dr. Lloyd Wilson</h3>
								<span class="position mb-2">Neurologiste</span>
								<div class="faded">
									<p>Le Dr. Wilson est expert en neurologie.</p>
									<ul class="ftco-social text-center">
		                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
		              </ul>
	              </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/doc-2.jpg);"></div>
							</div>
							<div class="text pt-3 text-center">
								<h3 class="mb-2">Dr. Rachel Parker</h3>
								<span class="position mb-2">Ophtalmologue</span>
								<div class="faded">
									<p>Le Dr. Rachel est une spécialiste en ophtalmologie.</p>
									<ul class="ftco-social text-center">
		                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
		              </ul>
	              </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/doc-3.jpg);"></div>
							</div>
							<div class="text pt-3 text-center">
								<h3 class="mb-2">Dr. Ian Smith</h3>
								<span class="position mb-2">Dentiste</span>
								<div class="faded">
									<p>Le Dr. Smith est un dentiste très expérimenté.</p>
									<ul class="ftco-social text-center">
		                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
		              </ul>
	              </div>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 ftco-animate">
						<div class="staff">
							<div class="img-wrap d-flex align-items-stretch">
								<div class="img align-self-stretch" style="background-image: url(images/doc-4.jpg);"></div>
							</div>
							<div class="text pt-3 text-center">
								<h3 class="mb-2">Dr. Alicia Henderson</h3>
								<span class="position mb-2">Pediatre</span>
								<div class="faded">
									<p>Le Dr. Henderson est une pediatre de renom.</p>
									<ul class="ftco-social text-center">
		                <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
		                <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
		              </ul>
	              </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		


    <section class="ftco-section bg-light" id="blog-section">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
          <div class="col-md-10 heading-section text-center ftco-animate">
            <h2 class="mb-4">Retrouvez tous les nouveaux articles ici</h2>
            <p>Chaque article provient de sources fiables.</p>
          </div>
        </div>
        <div class="row d-flex">
          <div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.html" class="block-20" style="background-image: url('images/image_1.jpg');">
              </a>
              <div class="text d-block">
              	<div class="meta mb-3">
                  <div><a href="https://francais.medscape.com/voirarticle/3605710">12 mars 2020</a></div>
                  <div><a href="https://francais.medscape.com/">MedScape</a></div>
                  <div><a href="https://francais.medscape.com/voirarticle/3605710" class="meta-chat"><span class="icon-chat"></span>0</a></div>
                </div>
                <h3 class="heading"><a href="https://francais.medscape.com/voirarticle/3605710">COVID-19 : les leçons viennent de la Chine, selon l’OMS</a></h3>
                <p>Genève, Suisse — A l’issue d’une mission internationale en Chine (du 16 au 24 février), l’Organisation Mondiale de la Santé (OMS) analyse les mesures qui ont permis d’endiguer la propagation du COVID-19 et qui font qu’aujourd’hui le pic épidémique est passé dans ce pays alors qu’il n’est pas encore atteint en Europe.
				</p>
                <p><a href="https://francais.medscape.com/voirarticle/3605710" class="btn btn-primary py-2 px-3">En savoir plus</a></p>
              </div>
            </div>
        	</div>

        	<div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.html" class="block-20" style="background-image: url('images/image_2.jpg');">
              </a>
              <div class="text d-block">
              	<div class="meta mb-3">
                  <div><a href="https://francais.medscape.com/voirarticle/3605716">10 mars 2020</a></div>
                  <div><a href="https://francais.medscape.com/voirarticle/3605716">Dr Claude Biéva</a></div>
                  <div><a href="https://francais.medscape.com/voirarticle/3605716" class="meta-chat"><span class="icon-chat"></span>0</a></div>
                </div>
                <h3 class="heading"><a href="https://francais.medscape.com/voirarticle/3605716">SARS-CoV-2 : a-t-il déjà muté?</a></h3>
                <p>Chine – Le nouveau coronavirus, alias SARS-CoV-2, a-t-il déjà muté ? Tout laisse penser que oui si l'on s'en réfère aux données publiées dans la National Science Revue ce 3 mars, rapportant l'identification de deux souches différentes désignées L et S au terme d'une comparaison des génomes de 103 échantillons du virus.</p>
                <p><a href="https://francais.medscape.com/voirarticle/3605716" class="btn btn-primary py-2 px-3">En savoir plus</a></p>
              </div>
            </div>
        	</div>

        	<div class="col-md-4 ftco-animate">
            <div class="blog-entry">
              <a href="blog-single.html" class="block-20" style="background-image: url('images/image_3.jpg');">
              </a>
              <div class="text d-block">
              	<div class="meta mb-3">
                  <div><a href="https://francais.medscape.com/voirarticle/3605712">10 mars 2020</a></div>
                  <div><a href="https://francais.medscape.com/voirarticle/3605712">Dr Claude Leroy</a></div>
                  <div><a href="https://francais.medscape.com/voirarticle/3605712" class="meta-chat"><span class="icon-chat"></span> 2</a></div>
                </div>
                <h3 class="heading"><a href="https://francais.medscape.com/voirarticle/3605712">Les laitages, toujours un bienfait pour la santé ?</a></h3>
                <p>Cela fait bien longtemps que le lait et ses dérivés sont globalement considérés comme bons pour la santé, et qu’ils sont à conseiller pour les enfants et la majorité des adultes.</p>
                <p><a href="https://francais.medscape.com/voirarticle/3605712" class="btn btn-primary py-2 px-3">En savoir plus</a></p>
              </div>
            </div>
        	</div>


    </section>

    <section class="ftco-section contact-section" id="contact-section">
      <div class="container">
      	<div class="row justify-content-center mb-5 pb-3">
          <div class="col-md-7 heading-section text-center ftco-animate">
            <h2 class="mb-4">Contactez nous</h2>
            <p>Vous trouverez ci-dessous différents moyens de nous contacter.</p>
          </div>
        </div>
        <div class="row d-flex contact-info mb-5">
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center bg-light">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-map-signs"></span>
          		</div>
          		<h3 class="mb-4">Notre siège social</h3>
	            <p>70 Boulevard Bessières, 75017 Paris</p>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center bg-light">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-phone2"></span>
          		</div>
          		<h3 class="mb-4">Notre numéro de téléphone</h3>
	            <p><a href="tel://1234567920">01 00 00 00 00</a></p>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center bg-light">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-paper-plane"></span>
          		</div>
          		<h3 class="mb-4">Adresse e-mail</h3>
	            <p><a href="mailto:NomDuSite@gmail.com">NomDuSite@gmail.com</a></p>
	          </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex ftco-animate">
          	<div class="align-self-stretch box p-4 text-center bg-light">
          		<div class="icon d-flex align-items-center justify-content-center">
          			<span class="icon-globe"></span>
          		</div>
          		<h3 class="mb-4">Site web</h3>
	            <p><a href="#">NomDuSite.fr</a></p>
	          </div>
          </div>
        </div>
       

  

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  
  <script src="js/main.js"></script>
    
  </body>
</html>